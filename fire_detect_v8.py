import cv2
from ultralytics import YOLO
import os
# import supervision as sv
from utils.torch_utils import select_device, time_sync
from utils.general import (LOGGER, Profile, check_file, check_img_size, check_imshow, check_requirements, colorstr, cv2,
                           increment_path, non_max_suppression, print_args, scale_boxes, strip_optimizer, xyxy2xywh)

from tqdm import tqdm
# from inference.models.yolo_world.yolo_world import YOLOWorld

cls = {0: 'fire', 1: 'smoke', 2: 'other'}
model_world = YOLO('custom_yolov8s.pt')
model_v8 = YOLO('best.pt')

# 计算iou
def calculate_iou(box1: list, box2: list):
    # 计算交集区域坐标
    x_inter1 = max(box1[0], box2[0])
    y_inter1 = max(box1[1], box2[1])
    x_inter2 = min(box1[2], box2[2])
    y_inter2 = min(box1[3], box2[3])

    # 计算交集区域的面积
    width_inter = max(0, x_inter2 - x_inter1)
    height_inter = max(0, y_inter2 - y_inter1)
    area_inter = width_inter * height_inter
    
    # 计算每个边界框的面积
    width_box1 = box1[2] - box1[0]
    height_box1 = box1[3] - box1[1]
    area_box1 = width_box1 * height_box1
    
    width_box2 = box2[2] - box2[0]
    height_box2 = box2[3] - box2[1]
    area_box2 = width_box2 * height_box2
    
    # 计算并集区域的面积
    area_union = area_box1 + area_box2 - area_inter
    
    # 计算 IOU
    iou = area_inter / area_union
    return iou

def compare_detections_2(detections1, detections2, iou_threshold=0.002):
    # result记录检测结果["fire", "smoke"]
    result, li= [], []
    # 两个模型都检测到火/烟
    if len(detections1[0].boxes.cls) != 0 and len(detections2[0].boxes.cls) != 0:
        n1 = len(detections1[0].boxes.cls)
        n2 = len(detections2[0].boxes.cls)
        # 优先遍历yolov8模型检测结果
        for i in range(n1):
            for j in range(n2):
                iou = calculate_iou(detections1[0].boxes.xyxy[i].tolist(), detections2[0].boxes.xyxy[j].tolist())
                if iou >= iou_threshold:
                     result.append(cls[detections1[0].boxes.cls[i].item()])
                    #  a = detections1[0].boxes.xyxy[i].tolist().copy()
                    #  a.append(detections1[0].boxes.conf[i].item())
                    #  li.append(a)
                     break
    if "fire" in result and "smoke" in result:
        return "open_flame"
    elif "fire" in result:
        return "open_flame"
    elif "smoke" in result:
        return "smoke_only"
    else:
        return None
    
def model(image_path, device="0"):
    yolov8_detections = model_v8.predict(source=image_path, conf=0.3, iou=0.4, device=device, classes=[0, 1])
    yolo_world_detections = model_world.predict(image_path, conf=0.05, iou=0.1, device=device) 
    result = compare_detections_2(yolov8_detections, yolo_world_detections)
    if result:
        return result, yolov8_detections[0].boxes
    else:
        return result
    
# 结果可视化
def draw_img(image, detections1, detections2):
    img = image.copy()
    if len(detections1[0].boxes.cls) != 0:
        for box1 in detections1[0].boxes.xyxy:
            bbox1 = [int(item) for item in box1]
            img = cv2.rectangle(img, (bbox1[0], bbox1[1]), (bbox1[2], bbox1[3]), (255, 0, 0), 2) #yolov8框为蓝色
    if len(detections2[0].boxes.cls) != 0:
        for box2 in detections2[0].boxes.xyxy:
            bbox2 = [int(item) for item in box2]
            img = cv2.rectangle(img, (bbox2[0], bbox2[1]), (bbox2[2], bbox2[3]), (0, 255, 0), 2) #yolo-world框为绿色
    return img


if __name__ == "__main__":
    i = 0
    image_path = "dataset/145600_uniview.jpg"
    # path = "/home/zhangjiwei/code/xizhan_baoan/yolov5/dataset/open_fire"
    t1 = time_sync()
    # for img in os.listdir(path):
    #     image_path = os.path.join(path, img)
    #     image = cv2.imread(image_path)
    # result = model(path)
    yolov8_detections = model_v8.predict(source=image_path, conf=0.3, iou=0.4, device="0", classes=[0, 1])
    yolo_world_detections = model_world.predict(image_path, conf=0.05, iou=0.1, device="0")     
    result = compare_detections_2(yolov8_detections, yolo_world_detections)
    if result:
        image = cv2.imread(image_path)
        img = draw_img(image, yolov8_detections, yolo_world_detections)
        cv2.imwrite(f"dataset/result/nofire/result+{i}.jpg", img)
        # cv2.imwrite(f"dataset/result/nofire/source+{i}.jpg", image)
        i += 1
    print(result)
    t2 = time_sync()
    LOGGER.info(f'({t2 - t1:.3f}s)')
    # print(i)