#!/bin/bash

while true
do
    proc=`ps aux | grep "Start_South_Person.py" | grep -v "grep" | wc -l`
    if [ $proc -le 0 ]; then
        ~/anaconda3/envs/wanghao/bin/python -u Start_South_Person.py >> nohup_preson.out &
    fi
    sleep 5
done
