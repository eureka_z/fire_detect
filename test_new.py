import cv2
import requests
import queue
import json
import mysql.connector
from datetime import datetime, timedelta  
from utils.torch_utils import select_device, time_sync
from unisense_monitor import wx_msg, wx_img

# result = {'camera_ip': '172.16.22.87', 'timestamp': 1710128825.029667,
#           'path': './dataset/172.16.1.10081300_uniview.jpg', 'noise': 0, 'preset': 1}

# res = {"result": result}
# json_dict = json.dumps(res)

# s = requests.session()
# s.keep_alive = False  # 关闭多余连接
# info = requests.post("http://localhost:18121/fire_detect", data=json_dict)
# print(info.json())

# 计算时间差
def calculate_time(datetime_str1, datetime_str2, second=70):
    # 将字符串转换为 datetime 对象
    if type(datetime_str1) == str:
        datetime_obj1 = datetime.strptime(datetime_str1, '%Y-%m-%d %H:%M:%S')
    else:
        datetime_obj1 = datetime_str1
    if type(datetime_str2) == str:
        datetime_obj2 = datetime.strptime(datetime_str2, '%Y-%m-%d %H:%M:%S')
    else:
        datetime_obj2 = datetime_str2

    # 计算时间差
    time_diff = abs(datetime_obj2 - datetime_obj1)

    # 定义一个 120 秒的 timedelta 对象
    threshold = timedelta(seconds=second)

    # 比较时间差是否小于 60 秒
    if time_diff < threshold:
        return 0
    else:
        return 1

def cvloc(req):
    json_dict = json.dumps(req)
    res = requests.post("http://172.17.1.89:6661/cv_loc", data=json_dict)
    if len(res.json()) == 0:
        return None
    return res.json()

def dedup(floor_id, cameras_in_one_group):
    req = {"floor_id": floor_id, "loc_data": cameras_in_one_group}
    json_dict = json.dumps(req)
    res = requests.post("http://172.17.1.89:6661/cv_dedup", data=json_dict)
    return res.json() 

def get_zonid(floorid, wx, wy):
    if floorid == 1:
        if 172.27 <= wx <= 383.62 and 411.43<= wy <=450.79:
            return "entrance"
        elif 108.77<=wx<=206.47 and 273.39<=wy<=333.29:
            return "pickup_1"
        elif 162.35<=wx<=342.18 and 335.34<= wy <=410.79:
            return "square"    
        elif 78.97<=wx<=170.84 and 388.54<=wy<=430.69:
            return "taxi"
    elif floorid == 2:
        if 117.47<=wx<=165.72 and 132.03<=wy<=166.92:
            return "south_stage"
        elif 163.99<=wx<=297.24 and 132.35<=wy<=139.48:
            return "outh_stage_corridor"
    elif floorid == 3:
        if 101.06<=wx<=139.03 and 68.00<=wy<=205.11:
            return "subway"
        elif 139.05<=wx<=150.85 and 68.00<=wy<=205.45:
            return "subway_corridor_east"
        elif 90.15<=wx<=101.02 and 68.00<=wy<=205.45:
            return "subway_corridor_west"
        elif 61.63<=wx<=177.85 and 205.45<=wy<=276.80:
            return "subway_exit_north"
        elif 90.15<=wx<=150.85 and 30.30<=wy<=68.00:
            return "subway_exit_south"
    return "other"


def write_into_db(info_list, floor, camera_res, path):
    dic = {"north_b2":3, "south_b1":2 , "xizhan_southern_square":1}
    floorid = dic[floor]
    heigth, width, ip = camera_res['img_h'], camera_res['img_w'], camera_res['camera_ip']
    fire_alarm_type = info_list[4]
    if fire_alarm_type == "smoke_only":
        status, heighest_alarm = 0, 0
    elif fire_alarm_type == "open_flame":
        status, heighest_alarm = 1, 1
    msg = ip + " " + fire_alarm_type

    for point in camera_res['points']:
        conf = point['confidence']
        if conf == 0:
            continue
        conf = str(conf)[:5]
        wx, wy, ts = point["wx"], point["wy"], point["ts"]
        if not wx or not wy:
            continue
        zoneid = get_zonid(floorid, wx, wy)
        # 建立与MySQL服务器的连接
        cnx = mysql.connector.connect(
            host="localhost",
            user="root",
            password="shuyuan_1234554322",
            database="db_events"
        )

        # 创建一个游标对象
        cursor = cnx.cursor()

        # 定义要执行的SQL插入语句
        sql1 = "INSERT INTO fire_alarm_events_test (create_ts, conf, alarm, zoneid, status, update_ts, heighest_alarm, floorid, process_ts, camera_ip, fire_alarm_type) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        # 如果ip一样且时间小于2min则认为是同一事件，不创建新事件，仅更新
        sql2 = """
        SELECT * FROM fire_alarm_events_test
        WHERE camera_ip = %s
        ORDER BY id DESC  # 假设'id'是可以表示顺序的列，如自增主键
        LIMIT 1
        """

        # 执行查询操作
        cursor.execute(sql2, (ip,))

        # 获取查询结果
        result = cursor.fetchone()
        # 当前ip之前发生过异常
        if result:
            # 上一次检测是第一次
            # if result[6] == result[0]:
            last_time = result[6]
            time = calculate_time(ts, last_time)
            if time == 0: # 时间差小于120s，更新
                wx_msg(msg)
                wx_img(path)
                # 获取要更新的新数据
                update_ts = ts
                # 定义更新语句
                update_query = "UPDATE fire_alarm_events_test SET conf=%s, zoneid=%s, status=%s, update_ts = %s, floorid=%s, fire_alarm_type=%s WHERE id = %s"
                # 执行更新语句
                cursor.execute(update_query, (conf, zoneid, status, update_ts, floor, fire_alarm_type, result[7]))
                
            else: #时间大于120s，插入新事件
                if fire_alarm_type == "open_flame":
                    break
                # 定义插入语句
                wx_msg(msg)
                wx_img(path)
                data = [
                    (ts, conf, None, zoneid, status, ts, heighest_alarm, floor, None, ip, fire_alarm_type)
                ]
                # 执行插入操作
                cursor.executemany(sql1, data)
            # else: # 上次检测更新过
            #     last_time = result[6]
            #     time = calculate_time(ts, last_time)
            #     if time == 0: # 时间差小于120s，更新
            #         update_ts = ts
            #         # 定义更新语句
            #         update_query = "UPDATE fire_alarm_events SET threshold=%s, zoneid=%s, status=%s, update_ts = %s, heighest_alarm=%s, floorid=%s, fire_alarm_type=%s WHERE id = %s"
            #         # 执行更新语句
            #         cursor.execute(update_query, (conf, zoneid, status, update_ts,heighest_alarm, floorid, fire_alarm_type,result[7]))
            #     else: #时间大于120s，插入新事件
            #         data = [
            #             (ts, None, conf, None, zoneid, status, ts, heighest_alarm, floorid, None, ip, fire_alarm_type)
            #         ]
            #         # 执行插入操作
            #         cursor.executemany(sql1, data)
        else: # 当前ip没发生过异常
            if fire_alarm_type == "open_flame":
                break
        # 创建新事件
            wx_msg(msg)
            wx_img(path)
            data = [
                (ts, conf, None, zoneid, status, ts, heighest_alarm, floor, None, ip, fire_alarm_type)
            ]
                
            # 执行插入操作
            cursor.executemany(sql1, data)
        # 提交更改到数据库
        cnx.commit()

        # 关闭数据库连接
        cursor.close()
        cnx.close()

def run(result):
    image_path = result["path"]
    res = {"result": result}
    json_dict = json.dumps(res)

    s = requests.session()
    s.keep_alive = False  # 关闭多余连接
    info = requests.post("http://localhost:18121/fire_detect", data=json_dict)
    info_list = info.json()
    ts = info_list[0]
    ip = info_list[1]
    height = info_list[2]
    width = info_list[3]
    if info_list[4] == 0:
        return None

    res = {"camera_ip" : ip, "img_w" : width, "img_h" : height, "is_head" : 0, "height" : None, "points" : []}
    for li in info_list[-1]:
        center_x = li[0]
        center_y = li[1]
        conf = li[-1]
        res["points"].append({"ptx" : center_x, "pty" : center_y, "confidence" : conf})
    # 定位
    loc = cvloc(res)
    for point in loc["points"]:
        point["ts"] = ts
    if len(loc["points"]) == 1:
        dic = {'acc': 0,
        'confidence': 0,
        'ptx': 0,
        'pty': 0,
        'wx': 0,
        'wy': 0,
        'ts': ts}
        loc["points"].append(dic)

    # 去重
    cameras_in_floor = {}
    dedup_res = []
    if loc != None:
        if loc['floor_id'] not in cameras_in_floor:
            cameras_in_floor[loc['floor_id']] = []
        cameras_in_floor[loc['floor_id']].append(loc)
    for floor, cameras in cameras_in_floor.items():
        #调用去重服务
        res = dedup(floor, cameras)
        dedup_res.extend(res['loc_data'])
        
    # 写入数据库
    for camera_res in dedup_res:
        floor = res["floor_id"]
        write_into_db(info_list, floor, camera_res, image_path)


if __name__ == "__main__":
    t3 = time_sync()
    result = {'camera_ip': '172.16.1.13', 'timestamp': 1710128945.029667,
            'path': 'dataset/_91753989_capture.jpg'}

    run(result)
    t4 = time_sync()
    print(f'({t4 - t3:.3f}s)')