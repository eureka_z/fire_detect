from gevent import monkey
from gevent.pywsgi import WSGIServer

monkey.patch_all()
import cv2
import os
import torch
from PIL import Image
# from engine import *
# from models import build_model
import time
from torch.multiprocessing import Process, get_context
import flask
import argparse
from flask import request
import json
import datetime
import numpy as np

app = flask.Flask(__name__)


def get_args_parser():
    parser = argparse.ArgumentParser('Set parameters for P2PNet evaluation', add_help=False)

    # * Backbone
    parser.add_argument('--backbone', default='vgg16_bn', type=str,
                        help="name of the convolutional backbone to use")

    parser.add_argument('--row', default=2, type=int,
                        help="row number of anchor points")
    parser.add_argument('--line', default=2, type=int,
                        help="line number of anchor points")

    parser.add_argument('--output_dir', default='./logs/',
                        help='path where to save')
    parser.add_argument('--weight_path', default='./weights/SHTechA.pth',
                        help='path where the trained weights saved')

    parser.add_argument('--gpu_id', default=0, type=int, help='the gpu used for evaluation')

    return parser


parser = argparse.ArgumentParser('P2PNet evaluation script', parents=[get_args_parser()])
args = parser.parse_args()
os.environ["CUDA_VISIBLE_DEVICES"] = '{}'.format(args.gpu_id)
device = torch.device('cuda')
# get the P2PNet
model = build_model(args)
# move to GPU
model.to(device)
if args.weight_path is not None:
    checkpoint = torch.load(args.weight_path, map_location='cpu')
    model.load_state_dict(checkpoint['model'])
# convert to eval mode
model.eval()
# create the pre-processing transform
transform = torch.transforms.Compose([
    torch.transforms.ToTensor(),
    torch.transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])
# transform = standard_transforms.Compose([
#     standard_transforms.ToTensor(),
#     standard_transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
# ])


@app.route("/crowd_counting", methods=["POST"])
def detect_person():
    if flask.request.method == "POST":
        json_data = request.get_data()
        params = json.loads(json_data)
        result = params['result']
    try:
        t1 = time.time()
        image = Image.open(result['path']).convert('RGB')
        images_timestamp = result['timestamp']
        camera_ip = result['camera_ip']
        width, height = image.size
        new_width = width // 128 * 128
        new_height = height // 128 * 128
        img_raw = image.resize((new_width, new_height), Image.LANCZOS)
        # pre-proccessing
        img = transform(img_raw)
        samples = torch.Tensor(img).unsqueeze(0)
        samples = samples.to(device)
        outputs = model(samples)
        outputs_scores = torch.nn.functional.softmax(outputs['pred_logits'], -1)[:, :, 1][0]
        outputs_points = outputs['pred_points'][0]
        threshold = 0.5
        # filter the predictions
        points = outputs_points[outputs_scores > threshold].detach().cpu().numpy().tolist()
        print(points)
        scores = outputs_scores[outputs_scores > threshold].detach().cpu().numpy().tolist()
        avg_def = np.average(scores)
        result_all_list = []
        if len(points) > 0:
            date_time = datetime.datetime.fromtimestamp(images_timestamp)
            result_all_list.append(datetime.datetime.strftime(date_time, "%Y-%m-%d %H:%M:%S"))
            result_all_list.append(camera_ip)
            result_all_list.append(len(scores))
            result_all_list.append(height)
            result_all_list.append(width)
            result_all_list.append(avg_def)
            for point, score in zip(points, scores):
                result_all_list.append([int(point[0]), int(point[1]), score])

        else:
            date_time = datetime.datetime.fromtimestamp(images_timestamp)
            result_all_list.append(datetime.datetime.strftime(date_time, "%Y-%m-%d %H:%M:%S"))
            result_all_list.append(camera_ip)
            result_all_list.append(0)
            result_all_list.append(height)
            result_all_list.append(width)
            result_all_list.append(0)
            result_all_list.append([0, 0, 0])
        return result_all_list

    except Exception as ex:
        print(ex)
        pass
    return 'ok'


def run(MULTI_PROCESS):
    if MULTI_PROCESS == False:
        WSGIServer(('0.0.0.0', 18120), app).serve_forever()
    else:
        mulserver = WSGIServer(('0.0.0.0', 18120), app)
        mulserver.start()

        def server_forever():
            mulserver.start_accepting()
            mulserver._stop_event.wait()

        for i in range(4):
            ctx = get_context("spawn")
            p = ctx.Process(target=server_forever)
            p.start()


if __name__ == "__main__":
    # 单进程 + 协程
    run(False)
    # 多进程 + 协程
    # run(True)
