# 南广场mqtt通讯文件
import json
import os
import cv2
from paho.mqtt import client as mqtt
import uuid

# 测试使用test_new
from test_new import run

# from Detector import Detector
from taosrest import connect
# head_detector = Detector('0', 1280)
conn = connect(url="http://localhost:6041", user="root", password="taosdata", timeout=30)

import datetime

# 导入 pix2world
# from world_point.pix2world import cv2w

def date_adjust(_date, time_delta):
    date_format = datetime.datetime.strptime(_date, "%Y-%m-%d %H:%M:%S")
    date_end = (date_format + datetime.timedelta(microseconds=time_delta)).strftime("%Y-%m-%d %H:%M:%S.%f")
    return date_end


def parse_ip(ip):
    if ip != None:
        ip = ip.replace(".", "_")
        return str(ip)


# def write_database(conn, cv_list):
#     """
#                  field              |          type          |   length    |    note    |
#     =====================================================================================
#      ts                             | TIMESTAMP              |           8 |            |
#      cv_total                       | INT                    |           4 |            |
#      ip                             | VARCHAR                |          64 |            |
#      xcoord                         | INT                    |           4 |            |
#      ycoord                         | INT                    |           4 |            |
#      confidence                     | FLOAT                  |           4 |            |
#      heigth                         | INT                    |           4 |            |
#      width                          | INT                    |           4 |            |

#     """

#     float_point = 0
#     values = ''
#     walues = ''
#     _ts = cv_list[0]
#     ip = cv_list[1]
#     cv_total = cv_list[2]
#     width = cv_list[3]
#     height = cv_list[4]

#     for _, elem in enumerate(cv_list[5:]):
#         xcoord = elem[0]
#         ycoord = elem[1]
#         confidence = elem[2]

#         values += str((date_adjust(_ts, float_point), cv_total, ip, xcoord, ycoord, confidence, height, width)) + ' '
#         try:
#             (wx, wy) = cv2w(ip, width, height, xcoord, ycoord)
#             walues += str((date_adjust(_ts, float_point), cv_total, ip, wx, wy, confidence, height, width)) + ' '
#         except:
#             pass
#         float_point += 1
    # print(values)

    # db_name = "db_counting_cv"
    # tb_name = "counting_cv_" + parse_ip(ip)
    # sql_create_tb = "CREATE TABLE " + " IF NOT EXISTS " + db_name + "." + tb_name + " (`ts` TIMESTAMP, `cv_total` INT, `ip` VARCHAR(64), `xcoord` INT, `ycoord` INT, `confidence` FLOAT, `heigth` INT, `width` INT)"
    # sql_insert_tb = "insert into db_counting_cv." + tb_name + " values " + values
    # sqls = []
    # sqls.append(sql_create_tb)
    # sqls.append(sql_insert_tb)
    # cursor = conn.cursor()

    # try:
    #     for sql in sqls:
    #         # print(sql)
    #         cursor.execute(sql)
    # except Exception as e:
    #     print("in exception:\t", sql)
    #     print("wrong insertion!", e)
    #     cursor.close()
    #     return
    # # else:
    # #     print("aaaaa")
    # #     cursor.close()

    # try:
    #     dbname = 'db_world_cv'
    #     tbname = 'world_cv_' + ip.replace('.', '_')
    #     sql_create_tb = 'CREATE TABLE IF NOT EXISTS {}.{} (`ts` TIMESTAMP, `cv_total` INT, `ip` VARCHAR(64), `xcoord` FLOAT, `ycoord` FLOAT, `confidence` FLOAT, `height` INT, `width` INT)'.format(dbname, tbname)
    #     sql_insert_tb = "insert into db_world_cv." + tbname + " values " + walues
    #     sqls = []
    #     sqls.append(sql_create_tb)
    #     sqls.append(sql_insert_tb)
    #     for sql in sqls:
    #         # print(sql)
    #         cursor.execute(sql)
    # except Exception as e:
    #     print('world', e)
    #     cursor.close()
    #     return
    # else:
    #     cursor.close()

class Subscription():

    def __init__(self, mt_ip, mt_user, mt_pwd):
        self.mt_ip = mt_ip
        self.mt_user = mt_user
        self.mt_pwd = mt_pwd
        self.mqttClient = mqtt.Client(str(uuid.uuid4()))

    def on_connect(self, client, userdata, flags, rc):
        """一旦连接成功, 回调此方法"""
        rc_status = ["连接成功", "协议版本不正确", "客户端标识符无效", "服务器不可用", "用户名或密码不正确", "未经授权"]
        print("connect：", rc_status[rc])

    def on_message(self, client, userdata, msg):
        """
        msg.payload.decode('utf8', 'ignore')：
        表示将 msg.payload 使用 GB2312 进行解码，并指定当遇到无法解码的字符时使用 ignore 参数进行错误处理，即忽略无法解码的字符

        msg.payload.decode('gb2312')：
        则是指定当遇到无法解码的字符时，抛出 UnicodeDecodeError 异常并停止程序运行
        """
        try:
            result = msg.payload.decode('utf-8', 'ignore')
            result = json.loads(result)
            if os.path.exists(result['path']):
                run(result)

                # if result['camera_ip'] in ['172.16.10.104', '172.16.10.110', '172.16.10.31','172.16.1.55']:
                #     print("********************Start****************************")
                #     print(result)
                #     image = cv2.imread(result['path'])
                #     preset = int(result['preset'])
                #     cv_list = head_detector.detect(image, result['camera_ip'], result['timestamp'])
                #     print("list_result", cv_list)
                #     if preset == 0:
                #         print('摄像头不在预置位，测试结果不会保存在数据库，请注意！！！')
                #     if len(cv_list) != 0 and preset != 0:
                #         write_database(conn, cv_list)
                #     print("********************End****************************")

        except Exception as ex:
            print(ex)
            pass

    def on_disconnect(self, userdata, rc):
        print("Disconnected with result code " + str(rc))
        self.mqttClient.reconnect()
        print("已重新连接！")

    def mqtt_connect(self):
        """连接MQTT服务器"""

        self.mqttClient.username_pw_set("admin", "bEjH_12XOM345_5xor4322_VONLINDAU")
        self.mqttClient.on_connect = self.on_connect  # 返回连接状态的回调函数
        self.mqttClient.on_message = self.on_message  # 返回订阅消息回调函数
        self.mqttClient.on_disconnect = self.on_disconnect
        # mqttClient.username_pw_set(self.mt_user, self.mt_pwd)  # MQTT服务器账号密码
        print(self.mt_ip)
        self.mqttClient.connect(self.mt_ip, 1883, 20)  # MQTT地址、端口、心跳间隔（单位为秒）
        # mqttClient.loop_start()  # 启用线程连接
        return self.mqttClient

    def on_subscribe(self):
        """订阅主题：mqtt/demo"""
        mqttClient = self.mqtt_connect()
        mqttClient.subscribe("fire", qos=0)
        mqttClient.loop_forever()
