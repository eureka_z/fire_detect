# a.py文件
import json
import os
import cv2
from paho.mqtt import client as mqtt
import uuid
# from Detector_xyxy import Detector
from taosrest import connect
import numpy as np
# from world_point.pix2world import *

head_detector = Detector('0', 1280)
conn = connect(url="http://172.17.1.89:6041", user="root", password="taosdata", timeout=30)

import datetime


def date_adjust(_date):
    date_time_s = datetime.datetime.fromtimestamp(_date)
    date_end = date_time_s.strftime("%Y-%m-%d %H:%M:%S.%f")
    return date_end


def parse_ip(ip):
    if ip != None:
        ip = ip.replace(".", "_")
        return str(ip)


def write_database(conn, cv_list):
    """
                 field              |          type          |   length    |    note    |
    =====================================================================================
     camera_ip                      | string                |           8 |            |
     ts                             | long                  |           4 |            |
     total                          | int                   |          64 |            |
     occupy                         | float                 |           4 |            |
    """

    # float_point = 0
    values = ''
    ip = cv_list[0]

    values += str((date_adjust(cv_list[1]), ip, cv_list[2], cv_list[3]))
    db_name = "db_passager_vehicle"
    tb_name = "passage_waiting_" + parse_ip(ip)
    sql_create_tb = "CREATE TABLE " + " IF NOT EXISTS " + db_name + "." + tb_name + " (`ts` TIMESTAMP,`ip` VARCHAR(64), `total` INT,`occupy` FLOAT)"
    sql_insert_tb = "insert into db_passager_vehicle." + tb_name + " values " + values
    sqls = []
    sqls.append(sql_create_tb)
    sqls.append(sql_insert_tb)
    cursor = conn.cursor()

    try:
        for sql in sqls:
            print(sql)
            cursor.execute(sql)
    except Exception as e:
        print("in exception:\t", sql)
        print("wrong insertion!", e)
        cursor.close()
        return
    else:
        print("aaaaa")
        cursor.close()


import urllib.request
from urllib.parse import urlparse


def is_web_url(path):
    parsed = urlparse(path)
    return parsed.scheme in ('http', 'https')


class Subscription():

    def __init__(self, mt_ip, mt_user, mt_pwd):
        self.mt_ip = mt_ip
        self.mt_user = mt_user
        self.mt_pwd = mt_pwd
        self.mqttClient = mqtt.Client(str(uuid.uuid4()))

    def on_connect(self, client, userdata, flags, rc):
        """一旦连接成功, 回调此方法"""
        rc_status = ["连接成功", "协议版本不正确", "客户端标识符无效", "服务器不可用", "用户名或密码不正确", "未经授权"]
        print("connect：", rc_status[rc])

    def on_message(self, client, userdata, msg):
        """
        msg.payload.decode('utf8', 'ignore')：
        表示将 msg.payload 使用 GB2312 进行解码，并指定当遇到无法解码的字符时使用 ignore 参数进行错误处理，即忽略无法解码的字符

        msg.payload.decode('gb2312')：
        则是指定当遇到无法解码的字符时，抛出 UnicodeDecodeError 异常并停止程序运行
        """
        try:
            result = msg.payload.decode('utf-8', 'ignore')
            result = json.loads(result)
            print("********************Start****************************")
            if is_web_url(result['path']):
                print('网页数据')
                resp = urllib.request.urlopen(result['path'])
                image = np.asarray(bytearray(resp.read()), dtype="uint8")
                # cv2.imdecode()函数将数据解码成Opencv图像格式
                image = cv2.imdecode(image, cv2.IMREAD_COLOR)
            else:
                image = cv2.imread(result['path'])
            preset = int(result['preset'])
            cv_list = head_detector.detect(image, result['camera_ip'], result['timestamp'])
            resultlist = [result['camera_ip'], result['timestamp'], len(cv_list), 0]
            print("list_result", resultlist)
            if preset == 0:
                print('摄像头不在预置位，测试结果不会保存在数据库，请注意！！！')
            if len(resultlist) != 0 and preset != 0:
                write_database(conn, resultlist)
            print("********************End****************************")


        except Exception as ex:
            print(ex)
            pass

    def on_disconnect(self, userdata, rc):
        print("Disconnected with result code " + str(rc))
        self.mqttClient.reconnect()
        print("已重新连接！")

    def mqtt_connect(self):
        """连接MQTT服务器"""
        self.mqttClient.on_connect = self.on_connect  # 返回连接状态的回调函数
        self.mqttClient.on_message = self.on_message  # 返回订阅消息回调函数
        self.mqttClient.on_disconnect = self.on_disconnect
        # mqttClient.username_pw_set(self.mt_user, self.mt_pwd)  # MQTT服务器账号密码
        print(self.mt_ip)
        self.mqttClient.connect(self.mt_ip, 1883, 20)  # MQTT地址、端口、心跳间隔（单位为秒）
        # mqttClient.loop_start()  # 启用线程连接
        return self.mqttClient

    def on_subscribe(self):
        """订阅主题：mqtt/demo"""
        mqttClient = self.mqtt_connect()
        mqttClient.subscribe('vehicle')
        mqttClient.loop_forever()
