import cv2
from ultralytics import YOLO
import os
# import supervision as sv
from utils.torch_utils import select_device, time_sync
from utils.general import (LOGGER, Profile, check_file, check_img_size, check_imshow, check_requirements, colorstr, cv2,
                           increment_path, non_max_suppression, print_args, scale_boxes, strip_optimizer, xyxy2xywh)

from tqdm import tqdm
# from inference.models.yolo_world.yolo_world import YOLOWorld

# # 获取模型
# model = YOLOWorld(model_id="yolo_world/l")
# # 设置类别
# classes = ["fire", "smoke", ""]
# model.set_classes(classes)

# def yolo_world_detect(image, conf=0.05, nms=0.1):
#     t1 = time_sync()
#     results = model.infer(image, confidence=conf)
#     detections = sv.Detections.from_inference(results).with_nms(threshold=nms)
#     t2 = time_sync()
#     # 输出结果
#     # LOGGER.info(f'({t2 - t1:.3f}s)')
#     return detections

model = YOLO('custom_yolov8s.pt')

def yolo_world_detect(image_path, conf=0.05, iou=0.1, device="0"):
    results = model.predict(image_path, conf=conf, iou=iou, device=device)
    return results


if __name__ == "__main__":
    path = '/home/zhangjiwei/code/xizhan_baoan/yolov5/dataset/open_fire/_91753989_capture.jpg'
    # img = cv2.imread(path)
    result = yolo_world_detect(path)
    print(result)