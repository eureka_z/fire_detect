#! /bin/bash
PRO_NAME="fire_server.py"
cmd=$1 #start/restart/stop
today=`date +"%Y-%m-%d"`
if [ "$cmd" = "start" ]
then
    nohup /home/traj/anaconda3/envs/yolov8/bin/python "${PRO_NAME}" &
    sleep 1
    nohup /home/traj/anaconda3/envs/yolov8/bin/python Start_South_Person.py &
    echo "start ${PRO_NAME} sucess"
elif [ "$cmd" = "stop" ]
then
    ps -ef | grep "Start_South_Person.py" | awk '{print $2}' | xargs kill -9 
    ps -ef | grep "${PRO_NAME}" | awk '{print $2}' | xargs kill -9
    echo "stop ${PRO_NAME} sucess"
elif [ "$cmd" = "restart" ]
then
    ps -ef | grep "${PRO_NAME}" | awk '{print $2}' | xargs kill -9    
    nohup /home/traj/anaconda3/envs/yolov8/bin/python "${PRO_NAME}" &
    sleep 1
    nohup /home/traj/anaconda3/envs/yolov8/bin/python Start_South_Person.py &
    echo "restart ${PRO_NAME} sucess"
else
    echo "do nothing"
fi
