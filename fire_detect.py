from yolov5_detect import detect
from yolo_world_detect import yolo_world_detect
import cv2
import os
import supervision as sv
from tqdm import tqdm
from utils.torch_utils import select_device, time_sync
from utils.general import (LOGGER, Profile, check_file, check_img_size, check_imshow, check_requirements, colorstr, cv2,
                           increment_path, non_max_suppression, print_args, scale_boxes, strip_optimizer, xyxy2xywh)

path = "/home/zhangjiwei/code/anomaly_detection/fire_smoke/yolo-world/image/nofire"

def calculate_iou(box1: list, box2: list):
    # 计算交集区域坐标
    x_inter1 = max(box1[0], box2[0])
    y_inter1 = max(box1[1], box2[1])
    x_inter2 = min(box1[2], box2[2])
    y_inter2 = min(box1[3], box2[3])

    # 计算交集区域的面积
    width_inter = max(0, x_inter2 - x_inter1)
    height_inter = max(0, y_inter2 - y_inter1)
    area_inter = width_inter * height_inter
    
    # 计算每个边界框的面积
    width_box1 = box1[2] - box1[0]
    height_box1 = box1[3] - box1[1]
    area_box1 = width_box1 * height_box1
    
    width_box2 = box2[2] - box2[0]
    height_box2 = box2[3] - box2[1]
    area_box2 = width_box2 * height_box2
    
    # 计算并集区域的面积
    area_union = area_box1 + area_box2 - area_inter
    
    # 计算 IOU
    iou = area_inter / area_union
    return iou

# 结果可视化
def draw_img(image, detections1, detections2):
    img = image.copy()
    if detections1:
        for detection in detections1:
            bbox =[int(item) for item in detection["position"]]
            img = cv2.rectangle(img, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (255, 0, 0), 2) #yolov5框为蓝色
    if len(detections2[0].boxes.cls) != 0:
        for box2 in detections2[0].boxes.xyxy:
            bbox2 = [int(item) for item in box2]
            img = cv2.rectangle(img, (bbox2[0], bbox2[1]), (bbox2[2], bbox2[3]), (0, 255, 0), 2) #yolo-world框为绿色
    return img


# yoloworld可视化
# def visualize(detections: sv.Detections, image):
#     BOUNDING_BOX_ANNOTATOR = sv.BoundingBoxAnnotator(thickness=2)
#     LABEL_ANNOTATOR = sv.LabelAnnotator(text_thickness=2, text_scale=1, text_color=sv.Color.BLACK)

#     annotated_image = image.copy()
#     annotated_image = BOUNDING_BOX_ANNOTATOR.annotate(annotated_image, detections)
#     annotated_image = LABEL_ANNOTATOR.annotate(annotated_image, detections)
#     # sv.plot_image(annotated_image, (10, 10))
#     # 将annotated_image以命名为与image_name同名并保存在result_fire路径下
#     # cv2.imwrite(f"results//{image_name}", annotated_image)
#     return annotated_image

def compare_detections(detections1, detections2, iou_threshold=0.002):
    # result记录检测结果["fire", "smoke"]
    result = []
    # 两个模型都检测到火/烟
    if detections1 and len(detections2.class_id) != 0:
        n1 = len(detections1)
        n2 = len(detections2.class_id)
        # 优先遍历yolov5模型检测结果
        for i in range(n1):
            for j in range(n2):
                iou = calculate_iou(detections1[i]["position"], detections2.xyxy[j])
                if iou >= iou_threshold:
                     result.append(detections1[i]["class"])
                     break
        if "fire" in result and "smoke" in result:
            return "fire and smoke"
        elif "fire" in result:
            return "fire"
        elif "smoke" in result:
            return "smoke"
        else:
            return None
        
# yolov8-world格式     
def compare_detections_2(detections1, detections2, iou_threshold=0.002):
    # result记录检测结果["fire", "smoke"]
    result = []
    # 两个模型都检测到火/烟
    if detections1 and len(detections2[0].boxes.cls) != 0:
        n1 = len(detections1)
        n2 = len(detections2[0].boxes.cls)
        # 优先遍历yolov5模型检测结果
        for i in range(n1):
            for j in range(n2):
                iou = calculate_iou(detections1[i]["position"], detections2[0].boxes.xyxy[j])
                if iou >= iou_threshold:
                     result.append(detections1[i]["class"])
                     break
    if "fire" in result and "smoke" in result:
        return "fire and smoke"
    elif "fire" in result:
        return "fire"
    elif "smoke" in result:
        return "smoke"
    else:
        return None

# 不使用iou
def compare_detections_3(detections1, detections2):
    result = []
    # 两个模型都检测到火/烟
    if detections1 and len(detections2.class_id) != 0:
        for i in range(len(detections1)):
            result.append(detections1[i]["class"])
        if "fire" in result and "smoke" in result:
            return "fire and smoke"
        elif "fire" in result:
            return "fire"
        elif "smoke" in result:
            return "smoke"
        else:
            return None

i = 0
t1 = time_sync()
for img in os.listdir(path):
    image_path = os.path.join(path, img)
    image = cv2.imread(image_path)
    yolov5_detections = detect(image)
    yolo_world_detections = yolo_world_detect(image_path)      
    result = compare_detections_2(yolov5_detections, yolo_world_detections)
    if result:
        img = draw_img(image, yolov5_detections, yolo_world_detections)
        cv2.imwrite(f"dataset/result/nofire/result+{i}.jpg", img)
        cv2.imwrite(f"dataset/result/nofire/source+{i}.jpg", image)
        i += 1
t2 = time_sync()
LOGGER.info(f'({t2 - t1:.3f}s)')
print(i)
                


        
        





