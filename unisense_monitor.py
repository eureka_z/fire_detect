import subprocess
from taosrest import connect
import json
import os
import time
import datetime
import base64
import hashlib

mobile_dict = {
    "wuhaifeng": "18210188205",
    "fengyahui": "18211069371",
    "wanghao": "15301091601",
}

south_stage_list = [
    "172.16.1.10",
    "172.16.1.15",
    "172.16.11.44",
    "172.16.11.46",
    "172.16.11.45",
    "172.16.22.98",
    "172.16.22.95",
    "172.16.22.100",
    "172.16.22.103",
    "172.16.1.88",
    "172.16.10.188",
    "172.16.10.187",
    "172.16.22.94",
    "172.16.11.49",
    "172.16.1.13",
    "172.16.22.87",
    "172.16.22.70",
    "172.16.22.20",
    "172.16.1.25",
    "172.16.11.50"]

def taos_query(sql_cmd, conn):
    """
    该函数调用

    输入：
    sql_cmd - sql 查询语句，字符串
    conn - taos connector

    输出：
    results - list, 查询结果
    """
    cursor_cmd = conn.cursor()
    try:
        cursor_cmd.execute(sql_cmd)
        results = cursor_cmd.fetchall()
        return results
    except Exception as e:
        print("wrong query!", e)
        cursor_cmd.close()
        return
    else:
        cursor_cmd.close()

def db_time(conn, db, table):
    sql_cmd = "select ts from {}.{} order by ts desc limit 1".format(db, table)
    db_ts = taos_query(sql_cmd, conn)
    print(type(db_ts[0][0]), db_ts[0][0])
    return db_ts[0][0]

def db_msg(conn, db, table, thres=300):
    current = datetime.datetime.now()
    print('current time:', current)
    db_ts = db_time(conn, db, table)
    print('db time:', db_ts)
    delta = (current - db_ts).total_seconds()
    # msg = "数据库 {}.{} 已经 {} 秒没有更新\n".format(db, table, delta)
    if delta > thres:
        msg = "数据库 {}.{} 已经 {} 秒没有更新\n".format(db, table, delta)
    else:
        msg = ''
    return msg

def db_monitor():
    global south_stage_list
    msg = ''
    conn = connect(url="http://localhost:6041",user="root",password="taosdata",timeout=30)
    msg += db_msg(conn, "db_westbahhof_validation", "wifi_counting_val", 150)
    msg += db_msg(conn, "db_westbahnhof_realtime", "wifi_counting_realtime_alex", 20)
    msg += db_msg(conn, "wifi_position", "pdata_wifi_position", 20)
    msg += db_msg(conn, "fusion_counting", "simple", 20)
    msg += db_msg(conn, "fusion_counting", "transratio", 1200)
    msg += db_msg(conn, "db_counting_cv", "counting_cv_172_16_10_104", 45)
    msg += db_msg(conn, "db_counting_cv", "counting_cv_172_16_10_110", 15)
    msg += db_msg(conn, "db_counting_cv", "counting_cv_172_16_10_31", 45)
    for ip in south_stage_list:
        msg += db_msg(conn, "db_counting_cv_south_stage",
                      "counting_cv_{}".format(ip.replace('.', '_')), 15)

    print(msg)
    wx_msg(msg, ["15301091601", "18211069371", "18210188205"])

def wx_msg(msg, at=None, group="monitor"):
    url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=ebb60f1d-cc2e-488c-8cc1-e10b5c824c1f"
    # if group == "monitor":
    #     url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=dc5004b3-b3bc-4f25-bfd7-185d596876a6'
    # elif group == "alarm":
    #     url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=1a0577a1-9588-4efe-bd90-2056413de59d'
    # else:
    #     # 默认用西站监控
    #     url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=dc5004b3-b3bc-4f25-bfd7-185d596876a6'

    if msg:
        wx_data = {
            "msgtype": "text",
            "text": {
                "content": time.strftime('%Y-%m-%d %H:%M', time.localtime(time.time())) + "\n" + msg,
                "mentioned_mobile_list": []
            }
        }

        if at:
            wx_data["text"]["mentioned_mobile_list"].extend(at)

        print(wx_data)

        curl_cmd = ['curl', '-k', url, '-H', 'Content-Type: application/json', '-d', json.dumps(wx_data)]
        curl_result = subprocess.run(curl_cmd, capture_output=True, universal_newlines=True)

def img2base64(path):
    # 将图片转换为base64编码
    with open(path, "rb") as image_file:
        encoded = base64.b64encode(image_file.read()).decode('utf-8')

    # 计算图片的MD5哈希值
    with open(path, "rb") as image_file:
        md5_hash = hashlib.md5(image_file.read()).hexdigest()

    return (encoded, md5_hash)

def wx_img(path, group="monitor"):
    url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=ebb60f1d-cc2e-488c-8cc1-e10b5c824c1f"
    # if group == "monitor":
    #     url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=dc5004b3-b3bc-4f25-bfd7-185d596876a6'
    # elif group == "alarm":
    #     url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=1a0577a1-9588-4efe-bd90-2056413de59d'
    # else:
    #     # 默认用西站监控
    #     url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=dc5004b3-b3bc-4f25-bfd7-185d596876a6'
    (b64, md5) = img2base64(path)
    wx_data = {
        "msgtype": "image",
        "image": {
            "base64": b64,
		    "md5": md5
        }
    }

    with open('tmp_img.txt', 'w') as f:
        f.write(json.dumps(wx_data))
    time.sleep(0.1)
    curl_cmd = ['curl', '-k', url, '-H', 'Content-Type: application/json', '-d', '@tmp_img.txt']
    curl_result = subprocess.run(curl_cmd, capture_output=True, universal_newlines=True)

def get_img(jsondata):
    # %Y-%m-%d %H:%M:%S
    timestr = jsondata['ts']


def disk_usage(disk_str):
    cmd = "df -h | grep {} | awk ".format(disk_str)
    cmd += "'{print $5}'"
    print(cmd)
    result = os.popen(cmd).read()
    print("disk result:", result)
    return result

def general_cmd(cmd):
    print(cmd)
    result = os.popen(cmd).read()
    print(result)
    return result

def nvidia_mon():
    result = general_cmd("nvidia-smi | sed 's/|/ /g' | grep Default | awk '{print $2, $7, $9, $10}'")
    msg = ""
    info = result.split()
    for k in range(4):
        msg += "GPU {}, 温度{}, 显存{}, 使用率{}\n".format(k, info[4 * k], info[4 * k + 1] + "/" + info[4 * k + 2], info[4 * k + 3])
    print(msg)
    return msg

def disk_monitor():
    msg = ''
    try:
        # 主硬盘使用率
        root_disk = disk_usage('nvme0n1p2')
        msg += "硬盘使用率 /：{}".format(root_disk)
        # /wifi 使用率
        wifi_disk = disk_usage('wifi')
        msg += "硬盘使用率 /wifi：{}".format(wifi_disk)
        # /traj 使用率
        traj_disk = disk_usage('traj')
        msg += "硬盘使用率 /traj：{}".format(traj_disk)
        # 内存
        msg += "可用内存：{}".format(general_cmd("free -h | grep 内存 | awk '{print $7}'"))
        # 负载
        msg += "CPU 负载：{}%\n".format(float(general_cmd("uptime | awk '{print $10}' | cut -d ',' -f 1"))/128*100)
        msg += nvidia_mon()
        wx_msg(msg)
    except Exception as e:
        print(e)
        pass

def wifi_rt_mon():
    msg = ''
    try:
        result = general_cmd("tail /home/shuyuan/wifi_realtime/nohup.out | head -n1")
        ts = float(result.split()[0])
        qsize = float(result.split()[1])
        current_ts = time.time()
        delta = current_ts - ts
        if delta > 30:
            msg += "WiFi 实时链路超时严重，延迟 {} 秒\n".format(delta)
        if qsize > 5000:
            msg += "WiFi 实时链路阻塞严重，队列阻塞 {} 条数据\n".format(qsize)
        wx_msg(msg, ["18210188205"])
    except Exception as e:
        print(e)
        pass

if __name__ == "__main__":
    msg = "火灾来啦快跑啊！"
    wx_msg(msg)
    # path 是火灾图片的绝对路径
    path = "xxx.jpg"
    wx_img(path)
